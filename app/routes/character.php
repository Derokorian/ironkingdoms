<?php

/**
 * routes for character pages
 */
$aRoutes[] = [
    'pattern' => '#^character/save/?#i',
    'controller' => 'App\Controller\CharacterSheetController',
    'dependencies' => ['App\Model\CharacterModel'],
    'method' => 'saveCharacter',
    'args' => []
];
$aRoutes[] = [
    'pattern' => '#^character/getCharacters/?#i',
    'controller' => 'App\Controller\CharacterSheetController',
    'dependencies' => ['App\Model\CharacterModel'],
    'method' => 'getCharacters',
    'args' => []
];
$aRoutes[] = [
    'pattern' => '#^character/getCareers/?#i',
    'controller' => 'App\Controller\CharacterSheetController',
    'dependencies' => ['App\Model\CharacterModel'],
    'method' => 'getCareers',
    'args' => []
];
$aRoutes[] = [
    'pattern' => '#^character/getRaces/?#i',
    'controller' => 'App\Controller\CharacterSheetController',
    'dependencies' => ['App\Model\CharacterModel'],
    'method' => 'getRaces',
    'args' => []
];
$aRoutes[] = [
    'pattern' => '#^character/getArchetypes/?#i',
    'controller' => 'App\Controller\CharacterSheetController',
    'dependencies' => ['App\Model\CharacterModel'],
    'method' => 'getArchetypes',
    'args' => []
];
$aRoutes[] = [
    'pattern' => '#^(character/?)?$#i',
    'controller' => 'App\Controller\CharacterSheetController',
    'dependencies' => ['App\Model\CharacterModel'],
    'method' => 'index',
    'args' => []
];
