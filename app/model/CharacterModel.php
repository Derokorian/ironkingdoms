<?php

/**
 * Character Model
 * @author Ryan Pallas
 * @package SampleSite
 * @namespace App\Model
 * @since 2014-02-27
 */
namespace App\Model;
use Dero\Data\BaseModel;
use Dero\Core\RetVal;
use Dero\Data\DataException;
use Dero\Data\DataInterface;
use Dero\Data\Factory;
use Dero\Data\ParameterCollection;

class CharacterModel extends BaseModel
{
    protected static $TABLE_NAME = 'character';

    protected static $COLUMNS = [
        'character_id' => [
            COL_TYPE => COL_TYPE_INTEGER,
            KEY_TYPE => KEY_TYPE_PRIMARY,
            'required' => false,
            'extra' => [
                DB_AUTO_INCREMENT
            ]
        ],
        'user_id' => [
            COL_TYPE => COL_TYPE_INTEGER,
            KEY_TYPE => KEY_TYPE_FOREIGN,
            'foreign_table' => 'users',
            'foreign_column' => 'user_id',
            'required' => true,
            'validation_pattern' => '/^[0-9]+$/'
        ],
        'name' => [
            COL_TYPE => COL_TYPE_STRING,
            'col_length' => 100,
            'required' => true,
            'validation_pattern' => '/^[a-z ]+$/i'
        ],
        'sex' => [
            COL_TYPE => COL_TYPE_FIXED_STRING,
            'col_length' => 1,
            'required' => false,
            'validation_pattern' => '/^(M|F)$/'
        ],
        'faith' => [
            COL_TYPE => COL_TYPE_STRING,
            'col_length' => 100,
            'required' => false,
            'validation_pattern' => '/^[a-z ]+$/i'
        ],
        'weight' => [
            COL_TYPE => COL_TYPE_STRING,
            'col_length' => 25,
            'required' => false,
            'validation_pattern' => '/^[0-9]+(lbs|kg)?$/'
        ],
        'height' => [
            COL_TYPE => COL_TYPE_STRING,
            'col_length' => 25,
            'required' => false,
            'validation_pattern' => '/^([1-6]+\'[0-9]{1,2}")|([1-9][0-9]*(in|")?)$/'
        ],
        'race_id' => [
            COL_TYPE => COL_TYPE_INTEGER,
            KEY_TYPE => KEY_TYPE_FOREIGN,
            'foreign_table' => 'race',
            'foreign_column' => 'race_id',
            'required' => false,
            'validation_pattern' => '/^[0-9]+$/'
        ],
        'archetype_id' => [
            COL_TYPE => COL_TYPE_INTEGER,
            KEY_TYPE => KEY_TYPE_FOREIGN,
            'foreign_table' => 'archetype',
            'foreign_column' => 'archetype_id',
            'required' => false,
            'validation_pattern' => '/^[0-9]+$/'
        ],
        'level' => [
            COL_TYPE => COL_TYPE_STRING,
            'col_length' => 25,
            'required' => false,
            'validation_pattern' => '/^[a-z0-9]+$/i'
        ],
        'xp' => [
            COL_TYPE => COL_TYPE_INTEGER,
            'col_length' => 10,
            'required' => false,
            'validation_pattern' => '/^[0-9]+$/i'
        ],
        'data' => [
            COL_TYPE => COL_TYPE_TEXT,
            'required' => false
        ],
        'created' => [
            COL_TYPE => COL_TYPE_DATETIME,
            'required' => false
        ],
        'modified' => [
            COL_TYPE => COL_TYPE_DATETIME,
            'required' => false
        ]
    ];

    private static $subObjects = [
        'race' => [
            'race_id' => [
                COL_TYPE => COL_TYPE_INTEGER,
                KEY_TYPE => KEY_TYPE_PRIMARY,
                'required' => false,
                'extra' => [
                    DB_AUTO_INCREMENT
                ]
            ],
            'name' => [
                COL_TYPE => COL_TYPE_STRING,
                'col_length' => 100,
                'required' => true
            ],
            'created' => [
                COL_TYPE => COL_TYPE_DATETIME,
                'required' => false
            ]
        ],
        'career' => [
            'career_id' => [
                COL_TYPE => COL_TYPE_INTEGER,
                KEY_TYPE => KEY_TYPE_PRIMARY,
                'required' => false,
                'extra' => [
                    DB_AUTO_INCREMENT
                ]
            ],
            'name' => [
                COL_TYPE => COL_TYPE_STRING,
                'col_length' => 250,
                'required' => true
            ],
            'created' => [
                COL_TYPE => COL_TYPE_DATETIME,
                'required' => false
            ]
        ],
        'archetype' => [
            'archetype_id' => [
                COL_TYPE => COL_TYPE_INTEGER,
                KEY_TYPE => KEY_TYPE_PRIMARY,
                'required' => false,
                'extra' => [
                    DB_AUTO_INCREMENT
                ]
            ],
            'name' => [
                COL_TYPE => COL_TYPE_STRING,
                'col_length' => 250,
                'required' => true
            ],
            'created' => [
                COL_TYPE => COL_TYPE_DATETIME,
                'required' => false
            ]
        ],
        'character_career' => [
            'character_id' => [
                COL_TYPE => COL_TYPE_INTEGER,
                KEY_TYPE => KEY_TYPE_FOREIGN,
                'required' => true,
                'foreign_table' => 'character',
                'foreign_column' => 'character_id'
            ],
            'career_id' => [
                COL_TYPE => COL_TYPE_INTEGER,
                KEY_TYPE => KEY_TYPE_FOREIGN,
                'required' => true,
                'foreign_table' => 'career',
                'foreign_column' => 'career_id'
            ],
            'created' => [
                COL_TYPE => COL_TYPE_DATETIME,
                'required' => false
            ]
        ]
    ];

    const CONCAT_SEPARATOR = '--|--';

    /**
     * Constructor
     */
    public function __construct($db = null)
    {
        if( !$db instanceof DataInterface )
            $db = Factory::GetDataInterface('default');
        parent::__construct($db);
    }

    public function VerifyTableDefinition()
    {
        $aRet = [];
        $sTable = self::$TABLE_NAME;
        $aCol = self::$COLUMNS;

        foreach( self::$subObjects as $strTable => $aCols )
        {
            if( $strTable == 'character_career' )
            {
                self::$TABLE_NAME = $sTable;
                self::$COLUMNS = $aCol;
                $oRetVal = parent::VerifyTableDefinition();
                if( $oRetVal->HasFailure() )
                {
                    $aRet[$strTable] = $oRetVal->GetError();
                }
                else
                {
                    $aRet[$strTable] = $oRetVal->Get();
                }

            }
            self::$TABLE_NAME = $strTable;
            self::$COLUMNS = $aCols;
            $oRetVal = parent::VerifyTableDefinition();
            if( $oRetVal->HasFailure() )
            {
                $aRet[$strTable] = $oRetVal->GetError();
            }
            else
            {
                $aRet[$strTable] = $oRetVal->Get();
            }
        }
        self::$TABLE_NAME = $sTable;
        self::$COLUMNS = $aCol;

        $oRetVal = new RetVal();
        $oRetVal->Set($aRet);

        return $oRetVal;
    }

    public function insertCharacter(&$oChar)
    {
        $aChar = (array) $oChar;
        $oRet = $this->validate($aChar);
        if( !$oRet->HasFailure() )
        {
            $aChar['data'] = json_encode($oChar);
            $oParams = new ParameterCollection();
            $strSql = 'INSERT INTO `character` ';
            $strSql .= $this->GenerateInsert($oParams, $aChar);
            try
            {
                $oRet->Set(
                    $this->DB
                        ->Prepare($strSql)
                        ->BindParams($oParams)
                        ->Execute()
                );
            } catch (DataException $e) {
                $oRet->AddError('Unable to query database', $e);
            }
        }
        if( !$oRet->HasFailure() )
        {
            $strSql = 'SELECT LAST_INSERT_ID()';
            try
            {
                $oRet->Set(
                    $this->DB
                        ->Prepare($strSql)
                        ->Execute()
                        ->GetScalar()
                );
            } catch (DataException $e) {
                $oRet->AddError('Unable to query database', $e);
            }
        }
        if( !$oRet->HasFailure() )
        {
            $oChar->character_id = $oRet->Get();
            $aRet = [];
            if( isset($oChar->careers) &&
                is_array($oChar->careers) )
            {
                foreach( $oChar->careers as $iCar )
                {
                    $oRet = $this->insertCharacterCareer($oChar->character_id, $iCar);
                    if( $oRet->HasFailure() )
                    {
                        $aRet[] = $oRet->GetError();
                    }
                    else
                    {
                        $aRet[] = $oRet->Get();
                    }
                }
            }
            $oRet->Set($aRet);
        }
        return $oRet;
    }

    public function insertCharacterCareer($iCharId, $iCareerId)
    {
        $oRet = new RetVal();
        $oParams = new ParameterCollection();
        $strSql = 'INSERT INTO `character_career` ';
        $strSql .= $this->GenerateInsert(
            $oParams,
            [
                'character_id' => $iCharId,
                'career_id' => $iCareerId
            ]
        );
        try
        {
            $oRet->Set(
                $this->DB
                    ->Prepare($strSql)
                    ->BindParams($oParams)
                    ->Execute()
            );
        } catch (DataException $e) {
            $oRet->AddError('Unable to query database', $e);
        }
        return $oRet;
    }

    public function deleteCharacterCareer($iCharId, $iCareerId)
    {
        $oRet = new RetVal();
        $oParams = new ParameterCollection();
        $strSql = 'DELETE FROM `character_career` ';
        $strSql .= $this->GenerateInsert(
            $oParams,
            [
                'character_id' => $iCharId,
                'career_id' => $iCareerId
            ]
        );
        try
        {
            $oRet->Set(
                $this->DB
                    ->Prepare($strSql)
                    ->BindParams($oParams)
                    ->Execute()
            );
        } catch (DataException $e) {
            $oRet->AddError('Unable to query database', $e);
        }
        return $oRet;
    }

    public function updateCharacter(&$oChar)
    {
        $aChar = (array) $oChar;
        $oRet = $this->validate($aChar);
        if( !$oRet->HasFailure() )
        {
            if( isset($aChar['data']) )
            {
                unset($aChar['data']);
            }
            $aChar['data'] = json_encode($oChar);
            $oParams = new ParameterCollection();
            $strSql = 'UPDATE `character` ';
            $strSql .= $this->GenerateUpdate($oParams, $aChar);
            try
            {
                $oRet->Set(
                    $this->DB
                        ->Prepare($strSql)
                        ->BindParams($oParams)
                        ->Execute()
                );
            } catch (DataException $e) {
                $oRet->AddError('Unable to query database', $e);
            }
        }
        if( !$oRet->HasFailure() )
        {
            $aRet = [];
            if( isset($oChar->careers) &&
                is_array($oChar->careers) )
            {
                $strSql = 'SELECT career_id FROM `character_career` ';
                $strSql .= $this->GenerateCriteria($oParams, ['character_id' => $oChar->character_id]);
                try
                {
                    $oRet->Set(
                        $this->DB
                            ->Prepare($strSql)
                            ->BindParams($oParams)
                            ->Execute()
                    );
                } catch (DataException $e) {
                    $oRet->AddError('Unable to query database', $e);
                }
                if( !$oRet->HasFailure() )
                {
                    $aCareerIds = [];
                    foreach( $oRet->Get() as $iCareerId )
                    {
                        $aCareerIds[] = $iCareerId;
                    }
                    foreach( $oChar->careers as $iCar )
                    {
                        if( !in_array($iCar, $aCareerIds) )
                        {
                            $oRet = $this->insertCharacterCareer($oChar->character_id, $iCar);
                            if( $oRet->HasFailure() )
                            {
                                $aRet[] = $oRet->GetError();
                            }
                            else
                            {
                                $aRet[] = $oRet->Get();
                            }
                        }
                        else
                        {
                            unset($aCareerIds[array_search($iCar, $aCareerIds)]);
                        }
                    }
                    foreach( $aCareerIds as $iCareerId )
                    {
                        $oRet = $this->deleteCharacterCareer($oChar->character_id, $iCareerId);
                        if( $oRet->HasFailure() )
                        {
                            $aRet[] = $oRet->GetError();
                        }
                        else
                        {
                            $aRet[] = $oRet->Get();
                        }
                    }
                }
            }
            $oRet->Set($aRet);
        }
        return $oRet;
    }

    public function getCharacter(Array $aOpts)
    {
        $oRet = new RetVal();
        $oParams = new ParameterCollection();
        $strSql = 'SELECT c.character_id, c.name, c.sex, c.weight, c.height, u.username,
                          c.faith, c.archetype_id, c.race_id, c.data, c.level, c.xp,
                          GROUP_CONCAT(
                              DISTINCT ca.career_id
                              ORDER BY ca.name ASC
                              SEPARATOR "' . self::CONCAT_SEPARATOR . '"
                          ) AS career_ids
                     FROM `character` c
                     JOIN `users` u ON u.user_id = c.user_id
                     LEFT JOIN `character_career` cca ON cca.character_id = c.character_id
                     LEFT JOIN `career` ca ON ca.career_id = cca.career_id '
                 . $this->GenerateCriteria($oParams, $aOpts, 'c.')
                 . ' GROUP BY c.character_id ';
        try
        {
            $oRet->Set(
                $this->DB
                    ->Prepare($strSql)
                    ->BindParams($oParams)
                    ->Execute()
                    ->GetAll()
            );
        } catch (DataException $e) {
            $oRet->AddError('Unable to query database', $e);
        }
        if( !$oRet->HasFailure() )
        {
            $aChars = $oRet->Get();
            foreach( $aChars as &$aChar )
            {
                $aChar = (array) $aChar;
                $aChar['career_ids'] = explode(self::CONCAT_SEPARATOR, $aChar['career_ids']);
                $aChar = array_replace_recursive($aChar, (array) json_decode($aChar['data'], JSON_OBJECT_AS_ARRAY));
                unset($aChar['data']);
                unset($aChar);
            }
            $oRet->Set($aChars);
        }
        return $oRet;
    }

    public function getCareers(Array $aOpts)
    {
        $oRet = new RetVal();
        $oParams = new ParameterCollection();
        $strSql = 'SELECT career_id, name FROM `career` ';
        $strSql .= $this->GenerateCriteria($oParams, $aOpts);
        try
        {
            $oRet->Set(
                $this->DB
                    ->Prepare($strSql)
                    ->BindParams($oParams)
                    ->Execute()
                    ->GetAll()
            );
        } catch (DataException $e) {
            $oRet->AddError('Unable to query database', $e);
        }
        return $oRet;
    }

    public function getRaces(Array $aOpts)
    {
        $oRet = new RetVal();
        $oParams = new ParameterCollection();
        $strSql = 'SELECT race_id, name FROM `race` ';
        $strSql .= $this->GenerateCriteria($oParams, $aOpts);
        try
        {
            $oRet->Set(
                $this->DB
                    ->Prepare($strSql)
                    ->BindParams($oParams)
                    ->Execute()
                    ->GetAll()
            );
        } catch (DataException $e) {
            $oRet->AddError('Unable to query database', $e);
        }
        return $oRet;
    }

    public function getArchetypes(Array $aOpts)
    {
        $oRet = new RetVal();
        $oParams = new ParameterCollection();
        $strSql = 'SELECT archetype_id, name FROM `archetype` ';
        $strSql .= $this->GenerateCriteria($oParams, $aOpts);
        try
        {
            $oRet->Set(
                $this->DB
                    ->Prepare($strSql)
                    ->BindParams($oParams)
                    ->Execute()
                    ->GetAll()
            );
        } catch (DataException $e) {
            $oRet->AddError('Unable to query database', $e);
        }
        return $oRet;
    }
}