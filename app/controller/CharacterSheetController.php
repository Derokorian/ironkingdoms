<?php

namespace App\Controller;
use App\Model\CharacterModel;
use Dero\Core\ResourceManager;
use Dero\Core\TemplateEngine;
use Dero\Core\BaseController;
use Dero\Core\Timing;

/**
 * CharacterSheet controller
 * @author Ryan Pallas
 * @package IronKingdoms
 * @namespace App\Controller
 * @since 2014-02-27
 */

class CharacterSheetController extends BaseController
{
    private $oCharModel;

    public function __construct(CharacterModel $oCharacterModel)
    {
        $this->oCharModel = $oCharacterModel;
    }

    public function index()
    {
        echo TemplateEngine::LoadView('header', ['title'=>'Index']);
        echo TemplateEngine::LoadView('character/main');
        echo TemplateEngine::LoadView('character/css-images');
        echo TemplateEngine::LoadView('footer');
    }

    public function saveCharacter()
    {
        Timing::start('controller');
        $aRet = [];
        $oChar = isset($_POST['character']) && is_array($_POST['character'])
            ? (object) $_POST['character'] : null;
        if( is_null($oChar) )
        {
            header('HTTP/1.1 422 Unprocessable Entity');
            $aRet['error'] = "No character information found";
        }
        elseif( !isset($_SESSION['user_id']) )
        {
            header('HTTP/1.1 403 Forbidden');
            $aRet['error'] = "You must be logged in to save characters";
        }
        else
        {
            $oChar->user_id = $_SESSION['user_id'];

            $oRet = $this->oCharModel->validate((array) $oChar);

            if( $oRet->HasFailure() )
            {
                header('HTTP/1.1 422 Unprocessable Entity');
                $aRet['error'] = $oRet->GetError();
            }
            else
            {
                if( isset($oChar->character_id) )
                {
                    $oRet = $this->oCharModel->updateCharacter($oChar);
                    if( $oRet->HasFailure() )
                    {
                        header('HTTP/1.1 422 Unprocessable Entity');
                        $aRet['error'] = $oRet->GetError();
                    }
                    else
                    {
                        $aRet['success'] = 'Successfully updated ' . $oChar->name;
                        $aRet['character'] = $oChar;
                    }
                }
                else
                {
                    $oChar->level = $oChar->level ?: "Hero";
                    $oChar->xp = $oChar->xp ?: 0;
                    $oRet = $this->oCharModel->insertCharacter($oChar);
                    if( $oRet->HasFailure() )
                    {
                        header('HTTP/1.1 422 Unprocessable Entity');
                        $aRet['error'] = $oRet->GetError();
                    }
                    else
                    {
                        header('HTTP/1.1 201 Created');
                        $aRet['success'] = 'Successfully added ' . $oChar->name;
                        $aRet['character'] = $oChar;
                    }
                }
            }
        }
        echo json_encode($aRet);
        header('x-controller-timing: '. Timing::end('controller'));
    }

    public function getCharacters()
    {
        $aOpts = [];
        $this->setFilter($aOpts, $_GET);
        if( isset($aOpts['id']) )
        {
            $aOpts['character_id'] = $aOpts['id'];
            unset($aOpts['id']);
        }
        $oRet = $this->oCharModel->getCharacter($aOpts);
        if( !$oRet->HasFailure() )
        {
            if( count($oRet->Get()) > 0 )
            {
                $aChars = array_map(function ($oChar) {
                    $aChar = (array) $oChar;
                    return $aChar;
                }, $oRet->Get());
                $c = count($aChars);
                $aRet = [
                    'success' => "Found $c characters",
                    'count' => $c,
                    'characters' => $aChars
                ];
            }
            else
            {
                $aRet = [
                    'success' => 'Found 0 characters',
                    'count' => 0,
                    'characters' => []
                ];
            }
        }
        else
        {
            header('HTTP/1.1 500 Internal Server Error');
            $aRet = [
                'error' => $oRet->GetError()
            ];
        }
        echo json_encode($aRet);
    }

    public function getRaces()
    {
        $aOpts = ['order_by' => 'name'];
        $this->setFilter($aOpts, $_GET);
        $oRet = $this->oCharModel->getRaces($aOpts);
        if( $oRet->HasFailure() )
        {
            header('HTTP/1.1 500 Internal Server Error');
            echo json_encode(['error' => $oRet->GetError()]);
            return;
        }
        echo json_encode($oRet->Get());
    }

    public function getCareers()
    {
        $aOpts = ['order_by' => 'name'];
        $this->setFilter($aOpts, $_GET);
        $oRet = $this->oCharModel->getCareers($aOpts);
        if( $oRet->HasFailure() )
        {
            header('HTTP/1.1 500 Internal Server Error');
            echo json_encode(['error' => $oRet->GetError()]);
            return;
        }
        echo json_encode($oRet->Get());
    }

    public function getArchetypes()
    {
        $aOpts = ['order_by' => 'name'];
        $this->setFilter($aOpts, $_GET);
        $oRet = $this->oCharModel->getArchetypes($aOpts);
        if( $oRet->HasFailure() )
        {
            header('HTTP/1.1 500 Internal Server Error');
            echo json_encode(['error' => $oRet->GetError()]);
            return;
        }
        echo json_encode($oRet->Get());
    }
}