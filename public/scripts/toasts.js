app.service('deroNotify', ['$timeout', '$document', function($timeout, $document) {
    var $this = this;
    this.getResultHandler = function(timeout) {
        var oMsg = {
            'duration': timeout
            };
        return function(oRet) {
            if( oRet.success ) {
                oMsg.message = oRet.success;
                oMsg.type = 'success';
            }
            else
            {
                var err = oRet.data.error;
                if( Array.isArray(err) )
                    err = err.join('<br />');

                oMsg.message = err;
                oMsg.type = 'danger';
            }

            $this.send(oMsg);
        };
    };

    this.send = function(oOpts) {
        if( oOpts === undefined ) return;
        var body = $document.find('body');

        var iDuration = oOpts.duration || 5000, // 5 second default
            sMessage = oOpts.message || '',
            sType = oOpts.type || 'info',
            sPosition = oOpts.position || 'top-right',
            sSize = oOpts.size || 'medium';

        // Make the element
        var eMessage = angular.element('<div></div>');
        eMessage.append(sMessage).addClass('notify').addClass('alert');

        // Position it
        if(['top-left','top-right','bottom-left','bottom-right'].indexOf(sPosition) > -1 ) {
            eMessage.addClass('notify-'+sPosition);
        } else {
            eMessage.addClass('notify-top-right');
        }

        // Colorize it
        if(['info','success','warning','danger'].indexOf(sType) > -1) {
            eMessage.addClass('alert-'+sType);
        } else {
            eMessage.addClass('alert-info');
        }

        // Size it
        if(['small','medium','large'].indexOf(sSize) > -1) {
            eMessage.addClass('notify-'+sSize);
        } else {
            eMessage.addClass('notify-medium');
        }

        // Verify duration
        if( typeof iDuration !== Number || parseInt(iDuration, 10) > 15000 || parseInt(iDuration, 10) < 500 ) {
            iDuration = 5000;
        } else {
            iDuration = parseInt(iDuration, 10);
        }

        // add it to the dom
        body.append(eMessage[0]);

        // Remove it after duration
        $timeout(function() {
            eMessage.remove();
        }, iDuration);
    }
}]);
