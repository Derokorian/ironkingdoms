app.controller('userCtrl', ['$scope', '$timeout', 'userModel', 'deroNotify', function($scope, $timeout, userModel, notify) {
    var resultHandler = notify.getResultHandler(5000);
    $scope.bIsRegistering = false;
    $scope.bForgotPassword = false;
    $scope.bIsLoggedIn = typeof $scope.user !== 'undefined';
    $scope.selectedCharacterId = null;

    $scope.$watch('selectedCharacterId', function(o,n) {
        if( o === n ) return;
        if( $scope.selectedCharacterId !== null )
        {
            $scope.user.characters.forEach(function(v) {
                if(v.character_id === $scope.selectedCharacterId )
                    $scope.$parent.setCharacter(v);
            });
        }
        else
        {
            $scope.$parent.setCharacter({});
        }
    })

    userModel.getCurrentUser().then(function(oRet) {
        resultHandler(oRet);
        if( oRet.success )
        {
            $scope.bIsLoggedIn = true;
            $scope.user = oRet.user;
        }
    })

    $scope.cancelRegister = function() {
        delete $scope.register;
        $scope.bIsRegistering = false;
    };

    $scope.cancelForgotPassword = function() {
        delete $scope.forgot;
        $scope.bForgotPassword =false;
    };

    $scope.submitRegister = function() {
        if( document.getElementById('frmRegister').checkValidity() )
        {
            userModel.saveUser($scope.register).then(function(oRet) {
                $scope.bIsRegistering = false;
                resultHandler(oRet);
            });
        }
    };

    $scope.submitForgotPassword = function() {
        if( document.getElementById('frmPasswordReset').checkValidity() )
        {
            userModel.forgotPassword($scope.forgot.email).then(function(oRet) {
                $scope.bForgotPassword = false;
                resultHandler(oRet);
            });
        }
    };

    $scope.logout = function() {
        userModel.logout().then(function(oRet) {
            resultHandler(oRet);
        });
        delete $scope.user;
        $scope.bIsLoggedIn = false;
        $scope.$parent.character = {};
    }

    $scope.doLogin = function() {
        if( document.getElementById('frmUserLogin').checkValidity() )
        {
            userModel.login($scope.login).then(function(oRet) {
                resultHandler(oRet);
                if( oRet.success )
                {
                    $scope.bIsLoggedIn = true;
                    $scope.user = oRet.user;
                }
            });
        }
    };

    function setResults(oRet) {
        if( oRet.success ) {
            $scope.$parent.results = oRet.success;
            $scope.$parent.isSuccess = true;
        }
        else
        {
            var err = oRet.error;
            if( Array.isArray(err) )
                err = err.join('<br />');
            $scope.$parent.results = err;
            $scope.$parent.isSuccess = false;
        }
        $timeout(function() {
            $scope.$parent.results = null;
        }, 3000);
    }
}]);