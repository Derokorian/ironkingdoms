app.controller('characterCtrl', ['$scope', '$http', 'characterModel', '$timeout', 'deroNotify', function($scope, $http, characterModel, $timeout, notify) {
    var resultHandler = notify.getResultHandler(5000);
    $scope.character = characterModel.getDefaultCharacter();

    $scope.setCharacter = function(oChar) {
        oChar = jQuery.extend(true, {}, characterModel.getDefaultCharacter(), oChar);
        $scope.character = oChar;
    }

    $scope.benefit_length = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19];
    $scope.gear_length = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18];

    $scope.options = {
        sex: [
            {
                value: 'M',
                text: 'Male'
            },
            {
                value: 'F',
                text: 'Female'
            }
        ]
    };

    characterModel.getRaces().then(function(arr) {
        $scope.options.races = arr;
    });
    characterModel.getCareers().then(function(arr) {
        $scope.options.careers = arr;
    });
    characterModel.getArchetypes().then(function(arr) {
        $scope.options.archetypes = arr;
    });

    var setDamage = function() {
        var bHit, o, i;
        for( o = 1; o <= 6; o++ ) {
            bHit = true;
            for( i = 0; i < 4; i++ ) {
                if( $scope.character.damage[o][i] === false ||
                    typeof $scope.character.damage[o][i] == 'undefined' )
                {
                    bHit = false;
                }
                $scope.character.damage[o][i] = bHit;
            }
        }

        for( o = 1; o < 6; o += 2 ) {
            bHit = true;
            if( $scope.character.damage[o][o == 1 ? 3 : 2] === false &&
                $scope.character.damage[o+1][o == 1 ? 3 : 2] === false )
            {
                bHit = false;
            }
            for( i = (o == 1 ? 4 : 3); i < (o == 1 ? 6 : 5); i++ )
            {
                if( $scope.character.damage[o][i] === false ||
                    $scope.character.damage[o][i] === null  )
                {
                    bHit = false;
                }
                $scope.character.damage[o][i] = bHit;
            }
        }
    }

    $scope.$watchCollection('character.damage[1]', setDamage);
    $scope.$watchCollection('character.damage[2]', setDamage);
    $scope.$watchCollection('character.damage[3]', setDamage);
    $scope.$watchCollection('character.damage[4]', setDamage);
    $scope.$watchCollection('character.damage[5]', setDamage);
    $scope.$watchCollection('character.damage[6]', setDamage);

    $scope.saveCharacter = function() {
        characterModel.saveCharacter($scope.character).then(resultHandler, resultHandler);
    }
}]);
