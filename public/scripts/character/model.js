app.factory('characterModel', ['$http', 'BASE_URL', function($http, BASE_URL) {
    var getErr = function(result) {
        throw new Error(JSON.stringify(result));
    };
    return {
        getRaces: function() {
            return $http.get(BASE_URL + 'character/getRaces').then(function (result) {
                return result.data;
            }, getErr);
        },
        getCareers: function() {
            return $http.get(BASE_URL + 'character/getCareers').then(function (result) {
                return result.data;
            }, getErr);
        },
        getArchetypes: function() {
            return $http.get(BASE_URL + 'character/getArchetypes').then(function (result) {
                return result.data;
            }, getErr);
        },
        getCharacters: function(opts) {
            return $http.get(BASE_URL + 'character/getCharacters', { params: opts }).then(function (result) {
                if( result.data.success )
                {
                    return result.data.characters;
                }
                else
                {
                    return [];
                }
            });
        },
        saveCharacter: function(oChar) {
            return $http({
                url: BASE_URL + 'character/saveCharacter',
                method: 'POST',
                data: { character: oChar }
            }).then(function (result) {
                return result.data;
            });
        },
        getDefaultCharacter: function() {
            var i, ret = {
                damage: {
                    1: new Array(6),
                    2: new Array(4),
                    3: new Array(5),
                    4: new Array(3),
                    5: new Array(5),
                    6: new Array(3)
                },
                benefits: new Array(19),
                gear: new Array(18),
                armor: new Array(4),
                connections: new Array(4),
                mechanika: {
                    housing: new Array(4),
                    runeplate: new Array(3),
                    capacitor: new Array(3)
                },
                spells: new Array(7)
            };
            for( i = 0; i < 20; i++ ) {
                ret.benefits[i] = {
                    name: null,
                    desc: null,
                    page: null
                };
                if( i < 19 )
                    ret.gear[i] = {
                        name: null,
                        benefit: null
                    };
                if( i < 8 )
                    ret.spells[i] = {
                        name: null,
                        cost: null,
                        rng: null,
                        aoe: null,
                        pow: null,
                        up: null,
                        off: null,
                        notes: null
                    };
                if( i < 5 ) {
                    ret.armor[i] = {
                        name: null,
                        desc: null,
                        spd: null,
                        def: null,
                        arm: null
                    };
                    ret.connections[i] = {
                        name: null,
                        desc: null,
                        page: null
                    };
                    ret.mechanika.housing[i] = {
                        name: null,
                        runeplate: null,
                        capacitor: null,
                        notes: null
                    };
                }
                if( i < 4 ) {
                    ret.mechanika.runeplate[i] = {
                        name: null,
                        benefit: null
                    };
                    ret.mechanika.capacitor[i] = {
                        name: null,
                        charges: null
                    };
                }
            }
            return ret;
        }
    }
}]);